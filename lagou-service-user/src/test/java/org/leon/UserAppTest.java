package org.leon;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.leon.entity.Token;
import org.leon.org.leon.service.LoginTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = UserServiceApp.class)
@RunWith(SpringRunner.class)
public class UserAppTest {

    @Autowired
    private LoginTokenService loginTokenService;

    @Test
    public void testRegister() {
        final String email = "zombre449@163.com";
        loginTokenService.register(email, "hello kitty");

        Token tokenEntity = loginTokenService.checkEmailExisted(email);

        Assert.assertEquals(email, tokenEntity.getEmail());
    }
}
