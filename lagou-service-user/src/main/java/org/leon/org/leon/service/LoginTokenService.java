package org.leon.org.leon.service;

import org.leon.entity.Token;

import java.util.Optional;

public interface LoginTokenService {

    /**
     * 注册行为, 注册成功,返回一个 UUID
     */
    String register(String email, String password) ;

    /**
     * 判断指定邮箱是否已经注册
     */
    Token checkEmailExisted(String email);


    /**
     * 检查 邮箱/密码 是否匹配, 如果匹配就返回一个 UUID
     */
    Optional<String> checkEmailAndPassword(String email, String password);


    /**
     * 检查是否是有效 token, 返回对应的邮箱地址
     */
    String checkToken(String token);
}
