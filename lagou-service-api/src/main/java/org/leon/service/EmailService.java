package org.leon.service;


public interface EmailService {

    void sendMail(String email, String code) throws Exception;

}
