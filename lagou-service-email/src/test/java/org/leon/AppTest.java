package org.leon;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.leon.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = EmailServiceApp.class)
@RunWith(SpringRunner.class)
public class AppTest {

    @Autowired
    private EmailService emailService;


    @Test
    public void testSendMail(){
        try {
            emailService.sendMail("liuyinliang@wps.cn", "this is a code email");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
