package org.leon.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

@Service
@Slf4j
public class EmailServiceImpl implements EmailService {

    private JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String mailFrom;

    @Autowired
    public EmailServiceImpl(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void sendMail(String toEmail, String code) throws Exception {

        log.info("~~~~~~~~~~~~~~~~~~~ email from" + mailFrom);


        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(mailFrom);
        message.setTo(toEmail);
        message.setSubject("XX系统注册码");
        message.setText("XX 系统注册码 " + code + ", 有效时间 10 分钟");

        try {
            mailSender.send(message);
            log.info("简单邮件已经发送。");
        } catch (MailException e) {
            log.error("发送简单邮件时发生异常！", e);
            throw e;
        }
    }


}
