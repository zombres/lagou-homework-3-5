package org.leon.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.leon.sentinel.AuthCodeFallBack;
import org.leon.service.AuthCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/code")
public class AuthCodeController {

    private AuthCodeService authCodeService;

    @Autowired
    public AuthCodeController(AuthCodeService authCodeService) {
        this.authCodeService = authCodeService;
    }

    /**
     * ⽣成验证码并发送到对应邮箱
     * @return true/false
     */
    @PostMapping("/create/{email}")
    @SentinelResource(value = "sendEmail", blockHandlerClass = AuthCodeFallBack.class, blockHandler = "createAndSendBlocked")
    public Boolean createAndSend(@PathVariable("email") String email) {
        return authCodeService.createAndSendCode(email);
    }

    /**
     * 校验验证码是否正确，0:正确; 1:错误; 2:超时
     * @return 0/1/2
     */
    @GetMapping("/validate/{email}/{code}")
    public Integer checkCode(@PathVariable String email, @PathVariable String code) {
        return authCodeService.checkCode(email, code);
    }

}
