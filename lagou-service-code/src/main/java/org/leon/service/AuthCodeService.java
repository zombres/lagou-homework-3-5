package org.leon.service;

public interface AuthCodeService {

    /**
     * 生成验证码并发送到指定邮箱, 验证码需要入库缓存
     * @param email 指定邮箱
     * @return 返回 0-成功; -1-失败; 1-已注册
     */
    Boolean createAndSendCode(String email);

    /**
     * 验证指定邮箱的验证码: 0-正确 1-错误 2-超时
     */
    Integer checkCode(String email, String code);

}
