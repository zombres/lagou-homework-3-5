package org.leon;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.leon.service.AuthCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = CodeServiceApp.class)
@RunWith(SpringRunner.class)
public class AppTest {

    @Autowired
    private AuthCodeService authCodeService;

    @Test
    public void testCreateCode() {
        authCodeService.createAndSendCode("zombre@139.com");
    }

    @Test
    public void testCheckCode() {
        Integer ret = authCodeService.checkCode("zombre@139.com", "309711");
        Assert.assertEquals(1L, ret.longValue());
    }

}
