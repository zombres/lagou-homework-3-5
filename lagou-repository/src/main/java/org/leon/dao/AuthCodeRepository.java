package org.leon.dao;

import org.leon.entity.AuthCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AuthCodeRepository extends JpaRepository<AuthCode, Integer>, JpaSpecificationExecutor<AuthCode> {
}
