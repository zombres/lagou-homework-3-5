package org.leon.entity;


import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "lagou_auth_code")
@Data
public class AuthCode {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String email;

    private String code;

    @Column(name = "createtime")
    private Date createTime;

    @Column(name = "expiretime")
    private Date expireTime;

}
