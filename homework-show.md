

![1](images/1.png)

# email 模块

移除了Eureka、Feign、Bus 等模块，使用了 Nacos 作为注册中心以及配置中心，使用了 Dubbo 对外暴露服务，dubbo 使用 Nacos 作为注册中心

![2](images/2.png)

配置文件删除了 Eureka、Config 的配置，增加 Nacos、Dubbo 的配置

![3](images/3.png)

实现了独立接口模块中的业务接口，使用 Dubbo 的 `@Service`注解来暴露服务

![4](images/4.png)

# code 模块

依赖与 email 模块一致, 配置也类似，由于 Code 模块中使用了 Sentinel 作为流控方式，配置文件中增加了 Sentinel Dashboard 的配置

![5](images/5.png)

使用 Dubbo 包中的 `@Reference`注解来订阅 Dubbo 服务

![6](images/6.png)

在 Controller 入口处，使用 `@SentinelResource`注解来实现了触发 Sentinel 流控后的默认返回值

![7](images/7.png)

# User 模块

配置文件部分修改

![8](images/8.png)

Sentinel 流控部分

![9](images/9.png)

# Gateway 模块

Nacos 依赖添加

![10](images/10.png)

配置文件部分

![11](images/11.png)